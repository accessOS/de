// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.
// const {ipcRenderer} = require('electron')
//
// ipcRenderer.on('kill-app', {title: process.title, pid: process.pid})
window.$ = require('jquery')
window.jQuery = $
const EventEmitter = require('events');
let os = require('os')

window.Emitter = new EventEmitter()
Emitter.setMaxListeners(0)
window.appsCounter = 0

window.openApp = function(app, args) {
  if (!args) {
    args = {}
  }
  $('#DE').append(`<de-app app="${app}" args='${JSON.stringify(args)}'></de-app>`)
}

window.openFile = function(file) {
  let ext = file.slice(file.lastIndexOf('.')+1)
  let opener = 'texter'
  switch (ext) {
    case "mp3":
      opener = "player"
      break
  }
  if (["png", "svg", "jpg", "jpeg", "gif"].indexOf(ext)>=0) {
    opener = "imager"
  }
  window.openApp(opener, {path: file})
}

Emitter.on('openApp', openApp.bind(this))
Emitter.on('openFile', openFile.bind(this))

class Notification {
  constructor(title, options) {
    document.querySelector("notifications-panel").append(title, options)
  }
}

window.Notification = Notification

const mt = require("mousetrap")
mt.bind("shift+alt+p", function() {
  let w = require('electron').remote.getCurrentWindow()
  let {clipboard} = require("electron")
  let dateFormat = require("dateformat")
  let fs = require('fs-extra')
  w.capturePage(function(image) {
    clipboard.writeImage(image)
    fs.outputFile(os.homedir()+'/Изображения/Screenshot/' + dateFormat(new Date, 'dd-mm-yyyy - HH:MM:ss') + '.png', image.toPNG())
    new Notification("Готово!", {
      body: "Скриншот скопирован",
      icon: "fullscreen",
      duration: 2000
    })
  })
})

mt.bind("shift+alt+d", function() {
  let win = require('electron').remote.getCurrentWindow()
  console.log(win.isDevToolsOpened);
  (win.isDevToolsOpened()
    ? win.closeDevTools
    : win.openDevTools)()
})

let fs = require("fs-extra")
let config = fs.readJsonSync(__dirname + "/settings.json")
if (!config)
  config = {}

$(function() {
  $("#DE").css("background-image", `url("wallpapers/${config.wallpaper}")`)
}.bind())

Emitter.on('colorSchemeChanged', function(color) {
  window.config.primaryColor = color
  fs.outputJson(__dirname + '/settings.json', window.config)
})

Emitter.on('wallpaperChanged', function(wallpaper) {
  $("#DE").css("background-image", `url("wallpapers/${wallpaper}")`)
  window.config.wallpaper = wallpaper
  fs.outputJson(__dirname + '/settings.json', window.config)
})

window.config = config

function showContextMenu(data) {
  let x = data.offsetX+2
  let y = data.offsetY+2

  if (data.from_app) {
    let test_app = $(`de-app[data-pid=${data.pid}]`)[0].getElem()
    x = data.offsetX + parseInt(test_app.style.left)+2
    y = data.offsetY + parseInt(test_app.style.top)+32
  }

  let menu_data = data.menu

  if ($(window).width() - 200 < x) {
    x = x - 200
  }

  if ($(window).height() - 40 * menu_data.length < y) {
    y = y - 40 * menu_data.length + 4
    x = x - 8
  }

  console.log(x,y, data)

  $(document.body).find('#__context-menu').remove()

  let menu = $(`<div id="__context-menu"></div>`).css({
    left: x+"px",
    top: y+"px"
  }).appendTo(document.body)


  for (let k in menu_data) {
    let v = menu_data[k]
    v.icon = v.icon || ""
    console.log(v.icon)
    let item = $(`
        <div class="__context-menu-item __context-menu-item-checkbox">
        <iron-icon ${v.icon.length == 0
      ? 'style="display: none;"'
      : ""} icon=${v.icon}></iron-icon>
          ${v.type == "checkbox"
        ? `<paper-checkbox noink ${v.checked == true
          ? 'checked=1'
          : ''}
          ${v.icon.length != 0
            ? `<iron-icon icon=${v.icon}></iron-icon>`
            : 'false!'}
           name=${k}></paper-checkbox>`
        : ""} <label for=${k}>${v.name}</label>
        </div>`).appendTo(menu).click(function() {
      console.log("clickl")
      menu.remove();
      v.callback()
    })
  }
  Emitter.on("appActive", function() {
    console.log("appActive");
    menu.remove()
  })
}


var access = require('./access.js')

access.registerContextMenuListener(showContextMenu.bind(this))
