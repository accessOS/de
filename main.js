const electron = require('electron')
const {session, webContents} = require('electron')
// Module to control application life.
const app = electron.app
var path = require('path')

// Specify flash path, supposing it is placed in the same directory with main.js.
let pluginName
switch (process.platform) {
  case 'win32':
    pluginName = 'pepflashplayer.dll'
    break
  case 'darwin':
    pluginName = 'PepperFlashPlayer.plugin'
    break
  case 'linux':
    pluginName = 'libpepflashplayer.so'
    break
}
app.commandLine.appendSwitch('ppapi-flash-path', path.join(__dirname, pluginName))
// Module to create native browser window.
const BrowserWindow = electron.BrowserWindow

const url = require('url')
contents = Object()

let mainWindow

var ipcMain = electron.ipcMain

var listeners = []

ipcMain.on('context menu listener', function(e) {
  console.log("Sender", e.sender.send)

  listeners.push(e.sender)
}.bind(this))

ipcMain.on('callback click', function(s, cb) {
  listeners.map(function(v) {
    try {
      v.send('click-callback-' + cb)
    } catch (err) {
      listeners.pop(v)
    }
  }.bind(this))
  // ipcMain.send('click-callback-'+cb)
}.bind(this))

x = 0

var dwmanagerLoaded = {}

var downloadId = 0

// var jq = require('jquery')

function sendToAll(partition, channel, data) {
  // let window = {}
  // console.log(jq().extend)
  let _dwManager = JSON.parse(JSON.stringify(dwmanagerLoaded))
  for (let i in dwmanagerLoaded[partition]) {
    let v = dwmanagerLoaded[partition][i]
    console.log("Channel", channel, dwmanagerLoaded)
    let wc = webContents.fromId(v)
    console.log(typeof(wc))
    if (!wc) {
      _dwManager[partition].splice(dwmanagerLoaded[partition].indexOf(v), 1)
      console.log("SKIPPED", v,)
      // continue
    } else {
      webContents.fromId(v).send(channel, data)
      console.log("Sent:", dwmanagerLoaded, "Channel", channel)
    }
  }
  dwmanagerLoaded = JSON.parse(JSON.stringify(_dwManager))
}

function waitFor(event) {
  // ipcMain.once(event, function())
}

ipcMain.on('requestDownloadsManager', function(e, data) {
  console.log()
  console.log("REQUESTED:", e.sender.id, dwmanagerLoaded, data)
  if (dwmanagerLoaded[data] != undefined) {
    if (dwmanagerLoaded[data].indexOf(e.sender.id) >= 0) {} else {
      dwmanagerLoaded[data].push(e.sender.id)
    }
    console.log(data, "RETURNED")
    return
  } else {
    dwmanagerLoaded[data] = [e.sender.id]
  }
  console.log(data)

  let webviewSession = session.fromPartition(data);
  webviewSession.on('will-download', async function(ev, item, webContents) {
    ev.preventDefault()
    console.log('web-contents', webContents.id)
    webContents.send('will-download', ev, item.getURL())
  }.bind(this))
})

ipcMain.on('request create context menu', function(e, json) {
  let cb = []
  let i = 0
  let old_x = x
  while (i < json.menu.length) {
    i++
    cb.push(x + i)
  }
  // x+=
  json.callbacks = cb
  for (let i in json.callback) {
    json.callbacks.push(x + i)
  }

  x += json.menu.length
  console.log(json, json.menu.length, x)

  listeners.map(function(v) {
    try {
      v.send('create context menu', json)
    } catch (err) {
      listeners.pop(v)
    }
  }.bind(this))
  e.sender.send('callbacks init', json.callbacks)
}.bind(this))

function createWindow() {
  mainWindow = new BrowserWindow({width: 1890, height: 740})
  contents = mainWindow

  mainWindow.loadURL(url.format({
    pathname: path.join(__dirname, 'index.html'),
    protocol: 'file:',
    slashes: true
  }))

  mainWindow.webContents.openDevTools()

  mainWindow.on('closed', function() {
    mainWindow = null
  })
}

app.on('ready', createWindow)

app.on('window-all-closed', function() {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', function() {
  if (mainWindow === null) {
    createWindow()
  }
})
