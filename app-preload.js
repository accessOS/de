window.ipcRenderer = require('electron').ipcRenderer

class Notification {
  constructor (title, options) {
    ipcRenderer.sendToHost('Notification', title, options)
  }
}

window.Notification = Notification

ipcRenderer.sendToHost("appStarted", process.pid)

ipcRenderer.on('initArgs', function(e,args){
  console.log(args)
  window.__args = args
})

document.addEventListener("mousedown", function(){
  ipcRenderer.sendToHost("clicked-on-app")
})