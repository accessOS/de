module.exports = (() => {
  const Shell = require('nodeshell')
  const command = 'pactl'
  let Mixer = {}
  Mixer.sink = null

  var formatData = data => {
    let sincs = String(data).split('\n')
    for (let i = 0; i < sincs.length; i++) {
      Mixer.sink = sincs[i].includes('RUNNING') ? sincs[i][0] : Mixer.sink
    }
    return Mixer.sink
  }

  var getSync = () => {
    let $p = new Promise(resolve => {
      resolve(Mixer.sink)
    })
    let $c = Shell.exec(command, ['list', 'sinks', 'short'])
      .then(formatData)
    return Mixer.sink ? $p : $c
  }

  var setVolum = percent => {
    return getSync().then(sinc => ['set-sink-volume', sinc, `${Math.floor(percent)}%`])
  }

  Mixer.syncs = () => {
    return Shell.exec(command, ['list', 'sinks'])
      .then(resp => {
        var arr = []
        var str = String(resp)
        var regex = new RegExp(`Sink #[0-9]\n.*\n.*\n.*[^Descrition]`, 'g')
        parts = regex.exec(str)
        parts.forEach(p => {
          let id = /[0-9]/.exec(p)[0]
          let desc = /(Description: )\s*(.*)/.exec(p)[2]
          arr.push({ sink: id, description: desc })
        })
        return arr
      })
  }

  Mixer.get = () => {
    let arr = ['list', 'sinks']
    let promise = Shell.exec(command, arr)
    return getSync()
      .then(sinc => {
        return promise.then(resp => {
          var str = String(resp)
          if (str.indexOf("№")) {
            var arrSinc = str.split(`№`)
          } else {
            var arrSinc = str.split(`#`)
          }
          // console.log(arrSinc, arrSinc[2])
          var regex = new RegExp(`([0-9][0-9]*)\\%`)
          console.log(regex.exec(arrSinc[2]))
          return Number(regex.exec(arrSinc[2])[1])
        })
      })
  }

  Mixer.setSink = sink => {
    return Shell.exec(command, ['set-default-sink', sink])
      .then(data => {
        Mixer.sink = sink
      })
      .catch(err => {
        Mixer.sink = sink
        throw new Error(err)
      })
  }

  Mixer.set = percent => {
    return setVolum(percent).then(data => {
      Shell.exec(command, data)
    })
  }
  return Mixer
})()
